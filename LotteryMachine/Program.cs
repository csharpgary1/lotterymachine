﻿using System;
using System.Collections.Generic;
namespace LotteryMachine
{
    class Program
    {
        //全域變數放球list
        public static List<int> PrimeNumbers = new List<int>();

        static void Main(string[] args)
        {
            //傳入球
            BallUtils.maxAmountOfBalls1(100, PrimeNumbers);
            //洗球
            BallUtils.Shuffle(PrimeNumbers);
            //每秒輸出10個球
            BallUtils.GetOneNumber(PrimeNumbers);


        }
    }
}
